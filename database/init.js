const mongoose = require('mongoose');
const db = 'mongodb://localhost/blog';
const glob = require('glob');
const { resolve } = require('path');

exports.initSchemas = () => {
    glob.sync(resolve(__dirname, './schema', '**/*.js')).forEach(require);
}

exports.connectdb = () => {
    mongoose.set('useCreateIndex', true);
    mongoose.connect(db, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    let maxConnectTimes = 0;
    return new Promise((resolve, reject) => {
        mongoose.connection.on('disconnected', () => {
            console.log('db disconnected');
            if (maxConnectTimes <= 5) {
                maxConnectTimes++;
                mongoose.connect(db);
            }
            else {
                reject();
                throw new Error('database disconnected');
            }
        });
        mongoose.connection.on('error', () => {
            console.log('db error');
            if (maxConnectTimes <= 5) {
                maxConnectTimes++;
                mongoose.connect(db);
            }
            else {
                reject();
                throw new Error('database error');
            }
        });
        mongoose.connection.once('open', () => {
            console.log('db open');
            resolve();
        })
    })
}

