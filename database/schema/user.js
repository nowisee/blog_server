const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const userSchema = new Schema({
    username: {
        unique: true,
        type: String,
    },
    password: String,
    createAt: {
        type: Date,
        default: Date.now(),
    },
    lastLoginAt: {
        type: Date,
        default: Date.now(),
    },
    avatar: {
        type: String,
        default: 'defaultAvatar.jpg',
    },
    praisedCount: {
        type: Number,
        default: 0,
    },
    signature: {
        type: String,
        default: '',
    },
    collectionsCount: {
        type: Number,
        default: 0,
    },
    articleCount: {
        type: Number,
        default: 0,
    },
    followCount: {
        type: Number,
        default: 0,
    },
    fansCount: {
        type: Number,
        default: 0,
    }
});


mongoose.model('User', userSchema);