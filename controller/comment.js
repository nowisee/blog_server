const Router = require('koa-router');
const mongoose = require('mongoose');
const Comment = mongoose.model('Comment');
const User = mongoose.model('User');
const { getHeaderUserinfo } = require('../utils/token');
const ObjectId = mongoose.Types.ObjectId;

const router = new Router();

router.post('/createComment', async ctx => {
    let reqParams = ctx.request.body;
    let token = ctx.headers.authorization;
    let tokenPayload = getHeaderUserinfo(token);
    if (!tokenPayload) {
        ctx.body = {
            code: 800,
            message: "用户未登录",
        }
        return;
    }
    let commentEl = {
        articleId: reqParams.articleId,
        commentText: reqParams.commentText,
    }
    if(!reqParams.anonymous){
        commentEl.creator = ObjectId(tokenPayload.userId);
    }
    try {
        let result = await Comment.create(commentEl);
        ctx.body = {
            code: 200,
            data: result,
        }
    }
    catch (error) {
        ctx.body = {
            code: 900,
            message: "数据库操作出现异常",
        }
    }
})

router.get('/commentList', async ctx => {
    let articleId = ctx.query.articleId;
    try {
        let result = await Comment.aggregate([
            {
                $match: {
                    articleId: articleId,
                }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'creator',
                    foreignField: '_id',
                    as: 'creatorInfo',
                }
            },
            {
                $project: {
                    '_id': 0,
                    'createAt': 1,
                    'creatorInfo.title': 1,
                }
            }

        ]);
        console.log(result);
        ctx.body = {
            code: 200,
            data: result,
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: "数据库操作出现异常",
        }
    }
})

module.exports = router;