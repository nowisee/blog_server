const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;

const commentschema = new Schema({
    articleId: String,
    creator: ObjectId,
    createAt: {
        type: Date,
        default: Date.now(),
    },
    content: String,
})

mongoose.model('Comment', commentschema);