const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;

const followSchema = new Schema({
    userId: ObjectId,
    followId: ObjectId,
})

mongoose.model('Follow', followSchema);