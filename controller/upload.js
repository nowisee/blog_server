const Router = require('koa-router');
const router = new Router();
const fs = require('fs');
const path = require('path');

router.post('/upload', async ctx => {
    let file = ctx.request.files.file;
    let path = file.path;
    console.log(path);
    // let prefixStr = '/www/admin/localhost_80/wwwroot/serve/public/image/';
    // let fileName = path.replace(prefixStr, '');
    let reg = /[^\\]*\..*/;
    let fileName = path.match(reg)[0];
    console.log('fileName', fileName);
    ctx.body = {
        code: 200,
        data: {
            imgUrl: fileName,
        }
    }
})

module.exports = router;