const Router = require('koa-router');
const router = new Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Article = mongoose.model('Article');
const User = mongoose.model('User');
const Collections = mongoose.model('Collections');
const { getHeaderUserinfo } = require('../utils/token');

router.get('/', async ctx => {
    ctx.body = 'Article';
})
router.post('/createArticle', async ctx => {
    let req = ctx.request.body;
    let token = ctx.headers['authorization'];
    let tokenPayload = getHeaderUserinfo(token);
    if (!tokenPayload) {
        ctx.body = {
            code: 800,
            message: "用户未登录",
        }
        return;
    }
    try {
        let result = await Article.create({
            title: req.title,
            introduction: req.introduction,
            imgCover: req.imgCover,
            content: req.content,
            status: req.status,
            creator: ObjectId(tokenPayload.userId),
            creatorName: tokenPayload.username,
        });
        await User.updateOne({ _id: result.creator }, { $inc: { articleCount: 1 } });
        ctx.body = {
            code: 200,
            message: "创建成功",
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: "数据库操作出现异常",
        }
    }
})

router.get('/articleList', async ctx => {
    console.log('serve is ok');
    try {
        let result = await Article.aggregate([
            {
                $match: {
                    status: 1
                }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'creator',
                    foreignField: '_id',
                    as: 'creatorInfo',
                }
            },
            {
                $project: {
                    commentsCount: 1,
                    createAt: 1,
                    imgCover: 1,
                    introduction: 1,
                    collectedTimes: 1,
                    title: 1,
                    watched: 1,
                    _id: 1,
                    'creatorInfo.username': 1,
                    'creatorInfo.avatar': 1,
                }
            },
            {
                $unwind: '$creatorInfo',
            }
        ]);
        ctx.body = {
            code: 200,
            data: result,
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: "数据库查询出错",
        }
    }


})


router.get('/currentArticle', async ctx => {
    let articleId = ctx.query.articleId;
    try {
        await Article.updateOne({ _id: articleId }, { $inc: { watched: 1 } });
        let result = await Article.aggregate([
            {
                $match: {
                    _id: ObjectId(articleId),
                }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'creator',
                    foreignField: '_id',
                    as: 'creatorInfo',
                }
            },
            {
                $project: {
                    title: 1,
                    introduction: 1,
                    content: 1,
                    creator: 1,
                    imgCover: 1,
                    watched: 1,
                    commentsCount: 1,
                    collectedTimes: 1,
                    status: 1,
                    createAt: 1,
                    'creatorInfo.avatar': 1,
                    'creatorInfo.username': 1,
                }
            },
            {
                $unwind: '$creatorInfo',
            }
        ]);
        ctx.body = {
            code: 200,
            data: result,
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: '数据库操作出现异常',
        }
    }
})

router.get('/recommendArticle', async ctx => {
    let articleSize = Article.find({}).estimatedDocumentCount();
    console.log(articleSize);
    let randomNum = Math.floor(Math.random() * articleSize);
    console.log(randomNum);
    try {
        let result = Article.find({}).limit(1).skip(randomNum);
        ctx.body = {
            code: 200,
            data: result,
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: '数据库操作出现异常',
        }
    }
})

router.post('/changeArticleStatus', async ctx => {
    let articleId = ctx.request.body.articleId;
    let token = ctx.headers.authorization;
    let tokenPayload = getHeaderUserinfo(token);
    if (!tokenPayload) {
        ctx.body = {
            code: 801,
            message: '用户身份已过期。',
        }
        return;
    }
    try {
        await Article.updateOne({ _id: articleId }, { $inc: { status: -1 } });
        ctx.body = {
            code: 200,
            message: '修改成功',
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: '数据库操作出现异常',
        }
    }
})

router.post('/draftList', async ctx => {
    let token = ctx.headers['authorization'];
    let tokenPayload = getHeaderUserinfo(token);
    if (!tokenPayload) {
        ctx.body = {
            code: 800,
            message: "用户未登录",
        }
        return;
    }
    try {
        let result = await Article.find({ creator: tokenPayload.userId, status: 0 },{
            content: 1,
            imgCover: 1,
            introduction: 1,
            title: 1,
            _id: 1,
            createAt: 1,
        });
        ctx.body = {
            code: 200,
            data: result,
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: "数据库操作出现异常",
        }
    }
})

router.post('/updateDraft', async ctx => {
    let draft = ctx.request.body;
    let token = ctx.headers.authorization;
    let tokenPayload = getHeaderUserinfo(token);
    if (!tokenPayload) {
        ctx.body = {
            code: 800,
            messsage: '用户未登录',
        }
        return;
    }
    try {
        await Article.updateOne({ _id: draft._id }, draft);
        ctx.body = {
            code: 200,
            message: '更新草稿成功',
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: "数据库操作出现异常",
        }
    }

})

module.exports = router;