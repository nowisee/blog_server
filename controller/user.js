const Router = require('koa-router');
const fs = require('fs');
const mongoose = require('mongoose');
const { encrypt, decrypt, getHeaderUserinfo } = require('../utils/token.js')
const User = mongoose.model('User');
const Article = mongoose.model('Article');
const Follow = mongoose.model('Follow');
const Collections = mongoose.model('Collections');
const ObjectId = mongoose.Types.ObjectId;

let router = new Router();
router.post('/signin', async ctx => {
    let req = ctx.request.body;
    console.log(req);
    let username = req.username;
    let password = req.password;
    try {
        let result = await User.findOne({ username: username });
        if (result) {
            if (result.password === password) {
                let token = encrypt({
                    userId: result['_id'],
                    username: username,
                });
                ctx.set('Access-Control-Expose-Headers', 'Authorization');
                ctx.set('Authorization', token);
                ctx.body = {
                    code: 200,
                    message: '登录成功',
                }
            }
            else {
                ctx.body = {
                    code: 803,
                    message: '密码错误',
                }
            }

        }
        else {
            ctx.body = {
                code: 802,
                message: '账号不存在',
            }
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: '数据库操作出现异常',
        }
    }
});
router.get('/', async ctx => {
    ctx.body = '用户';
})
router.post('/signup', async ctx => {
    let req = ctx.request.body;
    // console.log(req);
    try {
        let result = await User.create({
            username: req.username,
            password: req.password,
        });
        // console.log(result);
        let token = encrypt({
            userId: result['_id'],
            username: result.username,
        });
        ctx.set('Access-Control-Expose-Headers', 'Authorization');
        ctx.set('Authorization', token);
        ctx.body = {
            code: 200,
            message: '注册成功。',
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: '数据库操作出现异常',
        }
    }
});

router.post('/userinfo', async ctx => {
    let token = ctx.headers['authorization'];
    let tokenPayload = getHeaderUserinfo(token);
    if (tokenPayload) {
        try {
            let result = await User.findOne({ _id: tokenPayload.userId }, {
                articleCount: 1,
                avatar: 1,
                praisedCount: 1,
                username: 1,
                signature: 1,
                collectionsCount: 1,
                followCount: 1,
                _id: 0,
            });
            ctx.body = {
                code: 200,
                data: result,
            }
        }
        catch (error) {
            console.log();
            ctx.body = {
                code: 900,
                message: "数据库操作出现异常"
            }
        }
    }
    else {
        ctx.body = {
            code: 800,
            message: "用户身份已过期，请重新登录",
        }
    }
})

router.post('/briefUerinfo', async ctx => {
    let token = ctx.headers.authorization;
    console.log(token);
    let tokenPayload = getHeaderUserinfo(token);
    try {
        let result = await User.findOne({ _id: tokenPayload.userId }, { username: 1, avatar: 1 });
        ctx.body = {
            code: 200,
            data: result,
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: "数据库操作出现异常",
        }
    }
})

router.post('/follow', async ctx => {
    let followId = ctx.request.body.followId;
    let token = ctx.headers.authorization;
    let tokenPayload = getHeaderUserinfo(token);
    // console.log(tokenPayload);
    // console.log(req);
    if (!tokenPayload) {
        ctx.body = {
            code: 800,
            messsage: '用户未登录',
        }
        return;
    }
    try {
        let result = await Follow.updateOne({
            userId: tokenPayload.userId,
            followId: followId,
        }, {
            userId: ObjectId(tokenPayload.userId),
            followId: ObjectId(followId),
        }, {
            upsert: true,
        });
        console.log('result___________________', result);
        let status = Boolean(result.upserted);
        if (status) {
            await User.updateOne({ _id: tokenPayload.userId }, { $inc: { followCount: 1, } });
            await User.updateOne({ _id: followId }, { $inc: { fansCount: 1, } });
        }
        ctx.body = {
            code: 200,
            status: status,
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: "数据库操作出现异常",
        }
    }
})

router.post('/cancelFollow', async ctx => {
    let followId = ctx.request.body.followId;
    let token = ctx.headers.authorization;
    let tokenPayload = getHeaderUserinfo(token);
    if (!tokenPayload) {
        ctx.body = {
            code: 801,
            message: '用户身份已过期',
        }
        return;
    }
    try {
        await Follow.deleteOne({
            userId: tokenPayload.userId,
            followId: followId,
        })
        await User.updateOne({ _id: tokenPayload.userId }, { $inc: { followCount: -1, } });
        await User.updateOne({ _id: followId }, { $inc: { fansCount: -1 } });
        ctx.body = {
            code: 200,
            message: '已取消关注',
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: '数据库操作出现异常',
        }
    }
})

router.post('/followList', async ctx => {
    let token = ctx.headers.authorization;
    let tokenPayload = getHeaderUserinfo(token);
    if (!tokenPayload) {
        ctx.body = {
            code: 800,
            messsage: '用户未登录',
        }
        return;
    }
    try {
        let result = await Follow.aggregate([
            {
                $match: {
                    userId: ObjectId(tokenPayload.userId),
                }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'followId',
                    foreignField: '_id',
                    as: 'followInfo',
                }
            },
            {
                $project: {
                    _id: 0,
                    userId: 1,
                    followId: 1,
                    'followInfo.avatar': 1,
                    'followInfo.username': 1,
                }
            },
            {
                $unwind: '$followInfo',
            }
        ]);
        // console.log(result);
        ctx.body = {
            code: 200,
            data: result,
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: '数据库操作出现异常',
        }
    }
})

router.post('/collect', async ctx => {
    let articleId = ctx.request.body.articleId;
    console.log(articleId);
    let token = ctx.headers.authorization;
    let tokenPayload = getHeaderUserinfo(token);
    if (!tokenPayload) {
        ctx.body = {
            code: 800,
            messsage: '用户未登录',
        }
    }
    try {
        let articleDetail = await Article.findOne({ _id: articleId });
        let UpdateResult = await Collections.updateOne({
            userId: tokenPayload.userId,
            articleId: articleId,
        }, {
            userId: ObjectId(tokenPayload.userId),
            articleId: ObjectId(articleId),
            creator: ObjectId(articleDetail.creator),
        }, { upsert: true });
        // console.log("result____________", result);
        if (UpdateResult.upserted) {
            await User.updateOne({ _id: tokenPayload.userId }, { $inc: { collectionsCount: 1 } });
            await Article.updateOne({ _id: articleId }, { $inc: { collectedTimes: 1 } });
        }
        ctx.body = {
            code: 200,
            status: Boolean(UpdateResult.upserted),
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: "数据库操作出现异常",
        }
    }
})

router.post('/cancelCollect', async ctx => {
    let articleId = ctx.request.body.articleId;
    let token = ctx.headers.authorization;
    let tokenPayload = getHeaderUserinfo(token);
    if (!tokenPayload) {
        ctx.body = {
            code: 801,
            message: '用户身份已过期',
        }
        return;
    }
    try {
        await Collections.deleteOne({ articleId: articleId, userId: tokenPayload.userId });
        await User.updateOne({ _id: tokenPayload.userId }, { $inc: { collectionsCount: -1 } });
        await Article.updateOne({ _id: articleId }, { $inc: { collectedTimes: -1 } });
        ctx.body = {
            code: 200,
            message: '已取消收藏。',
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: '数据库操作出现异常',
        }
    }
})

router.post('/collectionsList', async ctx => {
    let token = ctx.headers.authorization;
    let tokenPayload = getHeaderUserinfo(token);
    if (!tokenPayload) {
        ctx.body = {
            code: 800,
            messsage: '用户未登录',
        }
        return;
    }
    try {
        let result = await Collections.aggregate([
            { $match: { userId: ObjectId(tokenPayload.userId) } },
            {
                $lookup: {
                    from: 'users',
                    localField: 'creator',
                    foreignField: '_id',
                    as: 'creatorInfo',
                }
            },
            {
                $lookup: {
                    from: 'articles',
                    localField: 'articleId',
                    foreignField: '_id',
                    as: 'articleInfo',
                }
            },
            {
                $project: {
                    creator: 1,
                    articleId: 1,
                    'creatorInfo.avatar': 1,
                    'creatorInfo.username': 1,
                    'articleInfo.title': 1,
                }
            },
            { $unwind: '$creatorInfo' },
            { $unwind: '$articleInfo' },
        ]);
        ctx.body = {
            code: 200,
            data: result,
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: '数据库操作出现异常',
        }
    }
})

router.post('/myArticleList', async ctx => {
    let token = ctx.headers.authorization;
    let tokenPayload = getHeaderUserinfo(token);
    if (!tokenPayload) {
        ctx.body = {
            code: 800,
            messsage: '用户未登录',
        }
        return;
    }
    try {
        let result = await Article.aggregate([
            {
                $match: {
                    creator: ObjectId(tokenPayload.userId),
                    status: { $gte: 0 },
                }
            },
            {
                $project: {
                    articleId: '$_id',
                    title: 1,
                    introduction: 1,
                    createAt: 1,
                    status: 1,
                    _id: 0,
                }
            }
        ]);
        ctx.body = {
            code: 200,
            data: result,
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: '数据库操作出现异常',
        }
    }
})


router.post('/praise', async ctx => {
    let req = ctx.request.body;
    let token = ctx.headers['authorization'];
    let tokenPayload = getHeaderUserinfo(token);
    if (!tokenPayload) {
        ctx.body = {
            code: 800,
            message: "用户未登录",
        }
        return;
    }
    try {

    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: '数据库操作出现异常',
        }
    }
})

router.post('/setAccount', async ctx => {
    let req = ctx.request.body;
    let token = ctx.headers.authorization;
    let tokenPayload = getHeaderUserinfo(token);
    if (!tokenPayload) {
        ctx.body = {
            code: 801,
            message: '用户身份已过期',
        }
        return;
    }
    // 这里是一段烂代码，我还没有想到如何很好的解决这个问题。
    try {
        let userUpdateParams = {};
        if (req.username) {
            userUpdateParams.username = req.username;
        }
        if (req.avatar) {
            userUpdateParams.avatar = req.avatar;
        }
        if (req.signature) {
            userUpdateParams.signature = req.signature;
        }
        await User.updateOne({ _id: tokenPayload.userId }, userUpdateParams);
        ctx.body = {
            code: 200,
            message: '修改成功。',
        }
    }
    catch (error) {
        console.log(error);
        ctx.body = {
            code: 900,
            message: '数据库操作出现异常',
        }
    }
})

module.exports = router;