const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const articleSchema = new Schema({
    creator: String,
    creatorAvatar: String,
    content: String,
    attachment: String,
    comments: String,
    createAt: {
        type: Date,
        default: Date.now(),
    }
})