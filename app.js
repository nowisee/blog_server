const Koa = require('koa');
const app = new Koa();
const mongoose = require('mongoose');
const Router = require('koa-router');
const { connectdb, initSchemas } = require('./database/init.js');
const cors = require('koa2-cors');
const koaBody = require('koa-body');
const path = require('path');
const koaStatic = require('koa-static');

(async () => {
    await connectdb();
})();
initSchemas();

// cors,bodyParser需要放在router
app.use(cors());
app.use(koaStatic(__dirname, './public'));
app.use(koaBody({
    multipart: true,
    formidable:{
        uploadDir:path.join(__dirname,'public/image/'), // 设置文件上传目录
        keepExtensions: true,    // 保持文件的后缀
        maxFieldsSize:2 * 1024 * 1024, // 文件上传大小
      }
}))

const user = require('./controller/user.js');
const home = require('./controller/home.js');
const article = require('./controller/article.js');
const file = require('./controller/upload');
const comment = require('./controller/comment.js');

const router = new Router();
router.use('/user', user.routes());
router.use('/home', home.routes());
router.use('/article', article.routes());
router.use('/file', file.routes());
router.use('/comment', comment.routes());

app.use(router.routes());
app.use(router.allowedMethods());





app.use(async ctx => {
    ctx.set('hello', 'world');
    ctx.body = '<h1>hello Koa</h1>'
})

app.listen(3000, () => {
    console.log('server starting at port 3000 00000');
})