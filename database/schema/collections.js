const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;

const collectionsSchema = new Schema({
    userId: ObjectId,
    articleId: ObjectId,
    // title: String,
    // creatorName: String,
    // creatorAvatar: String,
    // introduction: String,
    creator: ObjectId,
});

mongoose.model('Collections', collectionsSchema);