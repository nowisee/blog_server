const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;

const articleSchema = new Schema({
    title: String,
    introduction: String,
    content: String,
    creator: ObjectId,
    imgCover: {
        type: String,
        default: '',
    },
    watched: {
        type: Number,
        default: 0,
    },
    commentsCount: {
        type: Number,
        default: 0,
    },
    tag: Array,
    collectedTimes: {
        type: Number,
        default: 0,
    },
    status: Number, //0草稿, 1已发布
    createAt: {
        type: Date,
        default: Date.now(),
    }
})

mongoose.model('Article', articleSchema);