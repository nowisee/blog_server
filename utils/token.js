const jwt = require('jsonwebtoken');
const secret = 'secret';
const expiresTime = '1d';
function encrypt(data) {
    return jwt.sign(data, secret, {
        expiresIn: expiresTime,
    })
};
function decrypt(token) {
    try {
        const data = jwt.verify(token, secret);
        return {
            token: true,
            data: data,
        }
    } catch (error) {
        return {
            token: false,
            data: error,
        }
    }
}


function getHeaderUserinfo(token) {
    let result = decrypt(token);
    if (!result.token) {
        return false;
    }
    else {
        return {
            userId: result.data.userId,
            username: result.data.username
        }
    }
}

module.exports = {
    encrypt,
    decrypt,
    getHeaderUserinfo,
}